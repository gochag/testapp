//
//  NetworkService.swift
//  TestApp
//
//  Created by Tarlan Hekimzade on 09.10.2021.
//


import Foundation

typealias NetworkServiceCompletion<T: Decodable> = ((Swift.Result<T, Error>) -> Void)


final class NetworkService: NSObject, URLSessionDelegate {
    
    func sendRequest<T: Decodable>(model: T.Type,  completion: @escaping NetworkServiceCompletion<T>) {
        
        let urlString = "https://89.208.230.60/test/item"
        guard let url = URL(string: urlString) else {return}
        
        let configuration = URLSessionConfiguration.default
        let session = URLSession(configuration: configuration, delegate: self, delegateQueue:OperationQueue.main)
        
        session.dataTask(with: url) { (data, response, error) in
            
            if let error = error {
                completion(.failure(error))
            }
            
            do {
                let obj = try JSONDecoder().decode(T.self, from: data!)
                completion(.success(obj))
                
            } catch {
                
                completion(.failure(error))
            }
        }.resume()
    }
    
    func urlSession(_ session: URLSession, didReceive challenge: URLAuthenticationChallenge, completionHandler: (URLSession.AuthChallengeDisposition, URLCredential?) -> Void) {
        completionHandler(URLSession.AuthChallengeDisposition.useCredential, URLCredential(trust: challenge.protectionSpace.serverTrust!))
    }
}
