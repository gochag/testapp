//
//  VideoCacheService.swift
//  TestApp
//
//  Created by Tarlan Hekimzade on 09.10.2021.
//

import Foundation

protocol CacheService {
    func loadData(with url: URL, type:ContentType, complition: @escaping ((Swift.Result<URL, Error>) -> Void))
}

final class CacheServiceImp: CacheService {
    
    private let fileSystemStorage: FileSystemStorage
    
    init(fileSystemStorage: FileSystemStorage) {
        self.fileSystemStorage = fileSystemStorage
    }
    
    
    private var requestTask: URLSessionTask?
    
    
    func loadData(with url: URL, type:ContentType, complition: @escaping ((Swift.Result<URL, Error>) -> Void)) {
        let fileName = self.convertURLtoName(url: url)
        
        self.fileSystemStorage.readFile(fileName: fileName) { [weak self] response in
            
            switch response {
            case .success(let fileModel):
                print("==? ", fileModel)
                guard fileModel.size != 0 else {
                    complition(.failure(AppErrors.FileDownloadError))
                    return
                }
                complition(.success(fileModel.path))
            case .failure:
                
                var isVideo = false
                if case ContentType.video = type {
                    isVideo = true
                }
                
                self?.dowloadTask(with: url, isVideo: isVideo) { result in
                        switch result {
                        case .success(let data):
                            self?.fileSystemStorage.writeFile(with: data,
                                                              fileName: fileName) {
                                savedResult in
                                switch savedResult {
                                case.success(let savedURL):
                                    complition(.success(savedURL))
                                case .failure(let error):
                                    complition(.failure(error))
                                }
                            }
                        case .failure(let error):
                            complition(.failure(error))
                        }
                    }
            }
        }
    }
    
    
    // MARK: Private
    
    private func dowloadTask(with url: URL, isVideo: Bool,  completion: ((Swift.Result<Data, Error>) -> Void)?) {
        self.requestTask = URLSession.shared.dataTask(with: url) { data, response, error in
            let type = isVideo ? "video" : "image"
            guard let httpURLResponse = response as? HTTPURLResponse, httpURLResponse.statusCode == 200,
                  let mimeType = response?.mimeType, mimeType.hasPrefix(type),
                  let data = data,
                  error == nil else {
                
                completion?(.failure(AppErrors.FileDownloadError))
                return
            }
            completion?(.success(data))
        }
        self.requestTask?.resume()
    }
    
    private func convertURLtoName(url: URL) -> String {
        var urlString = url.absoluteString
        urlString = urlString.replacingOccurrences(of: ":", with: "`", options: .literal, range: nil)
        urlString = urlString.replacingOccurrences(of: "/", with: "~", options: .literal, range: nil)
        return urlString
    }

}
