//
//  Item.swift
//  TestApp
//
//  Created by Tarlan Hekimzade on 09.10.2021.
//

import Foundation

/*
{
  "a": "status",
  "task_id": 744,
  "status": 2,
  "results": {
    "single": "https://talk.sensor-tech.ru/test/files/f5baac7bc482463c81461884e3e97a31.mp4",
    "split_v": "https://talk.sensor-tech.ru/test/files/419774ee31bd483896a4500de1b98fa8.mp4",
    "split_h": "https://talk.sensor-tech.ru/test/files/4d6b81942064485ab9cd84c6bb2d700b.mp4",
    "src": "https://talk.sensor-tech.ru/test/files/_____bc482463c81461884e3e97a31.mp4",
    "preview_image": "https://talk.sensor-tech.ru/test/files/822feeaad07e4ae998c9e552f10afafc.jpg"
  }
}
 */

struct ItemModel: Decodable {
    
    let taskId: Int?
    let status: Int?
    let results: ItemResultModel?
}

struct ItemResultModel: Decodable {
    let single: String?
    let splitVertical: String?
    let splitHorizontal: String?
    let src: String?
    let previewImage: String?
    
    enum CodingKeys: String, CodingKey {
        case single
        case src
        case splitVertical = "split_v"
        case splitHorizontal = "split_h"
        case previewImage = "preview_image"
    }
}
