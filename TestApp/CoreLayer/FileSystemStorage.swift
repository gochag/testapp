//
//  FileSystemStorage.swift
//  TestApp
//
//  Created by Tarlan Hekimzade on 09.10.2021.
//

import Foundation

protocol FileSystemStorage {
    
    func readFile(fileName:String,
                  completion: @escaping ((Swift.Result<FileModel, Error>) -> Void))
    func writeFile(with data: Data,
                   fileName:String,
                   completion: @escaping ((Swift.Result<URL, Error>) -> Void))
}

struct FileModel {
    let fileName: String
    let path: URL
    let data: Data?
    let size: Int
}

final class FileSystemStorageImp: FileSystemStorage {
    
    func readFile(fileName:String,
                  completion: @escaping ((Swift.Result<FileModel, Error>) -> Void)) {

        if let path = FileManager.default.urls(for: .cachesDirectory, in: .userDomainMask).first {

            let fileURL = path.appendingPathComponent(fileName)
            
            guard let data = try? Data(contentsOf: fileURL) else {
                completion(.failure(AppErrors.FileNotFound))
                return
            }
            let fileModel = FileModel(fileName: fileName,
                                      path: fileURL,
                                      data: data,
                                      size: data.count)
            completion(.success(fileModel))
        }
    }
    
    func writeFile(with data: Data,
                   fileName:String,
                   completion: @escaping ((Swift.Result<URL, Error>) -> Void)){

        if let path = FileManager.default.urls(for: .cachesDirectory, in: .userDomainMask).first {

            let fileURL = path.appendingPathComponent(fileName)
            
            do {
                try data.write(to: fileURL)
                completion(.success(fileURL))
            } catch {
                completion(.failure(AppErrors.FileNotFound))
            }
        }
    }
    
}
