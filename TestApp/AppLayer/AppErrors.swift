//
//  AppErrors.swift
//  TestApp
//
//  Created by Tarlan Hekimzade on 09.10.2021.
//

import Foundation

enum AppErrors: Error {
    case FileNotFound
    case FileDownloadError
    case customError(String?)
    
    
    var localizedDescription: String {
        switch self {
        case .FileNotFound:
            return "Файл не найден"
        case .FileDownloadError:
            return "Не удалось загрузить файл"
        case .customError(let message):
            return message ?? "Неизвестная ошибка"
        }
    }
}
