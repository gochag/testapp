//
//  Assembly.swift
//  TestApp
//
//  Created by Tarlan Hekimzade on 09.10.2021.
//

import UIKit

protocol AssemblyModel { }

protocol Assembly {
    static func buildModule() -> UIViewController
    static func buildModule(with model: AssemblyModel) -> UIViewController
}

extension Assembly {
    
    static func buildModule() -> UIViewController {
        fatalError("buildModule error")
    }
    
    static func buildModule(with model: AssemblyModel) -> UIViewController {
        fatalError("buildModule error")
    }
}

