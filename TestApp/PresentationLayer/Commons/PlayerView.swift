//
//  VideoPlayerView.swift
//  TestApp
//
//  Created by Tarlan Hekimzade on 09.10.2021.
//

import UIKit
import AVFoundation

final class PlayerView: UIView {
    
    private let videoView = VideoPlayerView()
    private var avPlayer = AVPlayer()
    private var playerLooper: AVPlayerLooper?
    
    override func layoutSubviews() {
        self.addSubview(self.videoView)
        self.videoView.frame = self.bounds
    }
    
    init() {
        super.init(frame: .zero)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setupPlayer(with model: CardModel) {
        let asset = AVURLAsset(url: model.fileURL)
        
        let item = AVPlayerItem(asset: asset)
        self.avPlayer = AVPlayer(playerItem: item)
        
        self.videoView.player = self.avPlayer
        self.avPlayer.volume = 1

    }
    
    func stop() {
        self.avPlayer.pause()
        self.avPlayer.seek(to: CMTimeMakeWithSeconds(0,preferredTimescale: 1000))
    }
    
    func play() {
        self.avPlayer.play()
    }
}
