//
//  PlayerView.swift
//  TestApp
//
//  Created by Tarlan Hekimzade on 09.10.2021.
//

import UIKit
import AVFoundation

final class VideoPlayerView: UIView {
    
    override class var layerClass: AnyClass {
        return AVPlayerLayer.self
    }
    
    var playerLayer: AVPlayerLayer? {
        return self.layer as? AVPlayerLayer
    }
    
    
    var player: AVPlayer? {
        get {
            return self.playerLayer?.player
        }
        set {
            self.playerLayer?.player = newValue
        }
    }
    
    
    
}
