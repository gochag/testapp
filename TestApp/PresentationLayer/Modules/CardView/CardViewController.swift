//
//  PagersViewController.swift
//  TestApp
//
//  Created by Tarlan Hekimzade on 08.10.2021.
//

import UIKit

protocol CardViewIntput {
    func didObtainContent(with model: CardModel)
    func didFailObtainContent(with error: AppErrors)
}

final class CardViewController: UIViewController {
    
    var presenter: CardViewPresenterIntput?
    var videoURL: URL?
    var needToPlay = false
    
    private var playerView = PlayerView()
    
    private lazy var imageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFill
        imageView.translatesAutoresizingMaskIntoConstraints = false
        return imageView
    }()
    
    private lazy var errorStack: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .vertical
        stackView.distribution = .fill
        stackView.alignment = .fill
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.isHidden = true
        return stackView
    }()
    
    private lazy var errorIcon: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()
    
    private lazy var errorlabel: UILabel = {
        let label = UILabel()
        label.textColor = .white
        label.font = UIFont.boldSystemFont(ofSize: 18)
        label.textAlignment = .center
        label.numberOfLines = 3
        return label
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.drawSelf()
        self.presenter?.obtainContent()
    }
    
    func setup(with model: CardModel) {
        self.playerView.setupPlayer(with: model)
    }
    
    func playVideo() {
        guard let _ = self.videoURL else {
            self.needToPlay = true
            return
        }
        self.playerView.play()
    }
    
    func stopVideo() {
        self.needToPlay = false
        guard let _ = self.videoURL else { return }
        self.playerView.stop()
    }
    
    func updateCardInfo() {
        self.presenter?.updateCardInfo()
    }
    
    //MARK: - Private functions
    
    private func drawSelf() {
        self.view.addSubview(self.playerView)
        self.view.addSubview(self.imageView)
        self.view.addSubview(self.errorStack)
        
        self.errorStack.addArrangedSubview(self.errorIcon)
        self.errorStack.addArrangedSubview(self.errorlabel)
        
        self.playerView.frame = self.view.bounds
        self.imageView.frame = self.view.bounds
        
        NSLayoutConstraint.activate(self.createErrorStack())
    }
    
    private func createErrorStack() -> [NSLayoutConstraint] {
        return [
            self.errorStack.leftAnchor.constraint(equalTo: self.view.leftAnchor),
            self.errorStack.rightAnchor.constraint(equalTo: self.view.rightAnchor),
            self.errorStack.centerYAnchor.constraint(equalTo: self.view.centerYAnchor),
            self.errorIcon.heightAnchor.constraint(equalToConstant: 80),
            self.errorIcon.widthAnchor.constraint(equalToConstant: 80)
        ]
    }
}

extension CardViewController: CardViewIntput {
    func didObtainContent(with model: CardModel) {
        
        DispatchQueue.main.async {
            switch model.contentModel.type {
            case .video:
                self.videoURL = model.fileURL
                self.setup(with:  model)
                
                if self.needToPlay {
                    self.playVideo()
                }
            case .photo:
                let image = UIImage(contentsOfFile: model.fileURL.path)
                self.imageView.image = image
            }
        }
    }
    
    func didFailObtainContent(with error: AppErrors) {
        DispatchQueue.main.async {
            self.errorStack.isHidden = false
            self.errorIcon.image = UIImage(named: "ic_error")
            self.errorlabel.text = error.localizedDescription
        }
    }
}
