//
//  PageViewPresenter.swift
//  TestApp
//
//  Created by Tarlan Hekimzade on 10.10.2021.
//

import Foundation

struct CardModel {
    let fileURL: URL
    var contentModel: ContentModel
}

protocol CardViewPresenterIntput  {
    func obtainContent()
    func updateCardInfo()
}

final class CardViewPresenter {
    var view: CardViewIntput?
    
    private let videoCacheService: CacheService
    private var model: ContentModel
    
    init(videoCacheService: CacheService,
         model: ContentModel) {
        self.videoCacheService = videoCacheService
        self.model = model
    }
    
}


//MARK: - PageViewPresenterIntput

extension CardViewPresenter: CardViewPresenterIntput {
    
    func obtainContent() {
        
        self.videoCacheService.loadData(with: self.model.url, type: model.type) { [weak self] response in
            guard let self = self else { return }
            
            switch response {
            case .success( let url):
                self.view?.didObtainContent(with: CardModel(fileURL: url, contentModel: self.model))
            case .failure(let error):
                print("error ==> ", error.localizedDescription)
                if let error = error as? AppErrors {
                    self.view?.didFailObtainContent(with: error)
                } else {
                    self.view?.didFailObtainContent(with: AppErrors.customError(nil))
                }
            }
        }
    }
    
    func updateCardInfo() {
        self.model.delegate.obtainMetaInfo(with: self.model.name, type: self.model.type)
    }
    
}
