//
//  PageViewAssembly.swift
//  TestApp
//
//  Created by Tarlan Hekimzade on 10.10.2021.
//

import UIKit

final class CardViewAssembly: Assembly {
    
    struct Model: AssemblyModel {
        let contentModel: ContentModel
        
    }
    
    static func buildModule(with model: AssemblyModel) -> UIViewController {
        guard let model = model as? Model else { fatalError() }
        let fileSystemStorage = FileSystemStorageImp()
        let videoCacheService = CacheServiceImp(fileSystemStorage: fileSystemStorage)
        let viewController = CardViewController()
        let presenter = CardViewPresenter(videoCacheService: videoCacheService, model: model.contentModel)
        viewController.presenter = presenter
        presenter.view = viewController
        return viewController
    }
    
}
