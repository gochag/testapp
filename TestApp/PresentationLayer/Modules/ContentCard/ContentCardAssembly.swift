//
//  ContentCardAssembly.swift
//  TestApp
//
//  Created by Tarlan Hekimzade on 09.10.2021.
//

import UIKit

final class ContentCardAssembly: Assembly {
    
    static func buildModule() -> UIViewController {
        let networkService = NetworkService()
        let provider = ContentCardProviderImp()
        
        let viewController = ContentCardViewController()
        let presenter = ContentCardPresenter(networkService: networkService, provider: provider)
        viewController.presenter = presenter
        presenter.view = viewController
        return viewController
    }
    
}
