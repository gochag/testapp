//
//  ContentCardPresenter.swift
//  TestApp
//
//  Created by Tarlan Hekimzade on 08.10.2021.
//

import Foundation

protocol ContentCardPresenterIntput: AnyObject {
    func obtainItem()
}

protocol CardInfoDelegate {
    func obtainMetaInfo(with filename: String, type: ContentType)
}

final class ContentCardPresenter {
    
    weak var view: ContentCardViewControllerIntput?
    
    private let networkService: NetworkService
    private let provider: ContentCardProvider
    
    
    init(networkService: NetworkService, provider: ContentCardProvider) {
        self.networkService = networkService
        self.provider = provider
    }
    
}


//MARK: - ContentCardPresenterIntput

extension ContentCardPresenter: ContentCardPresenterIntput {
    
    func obtainItem() {
        self.networkService.sendRequest(model: ItemModel.self) { [weak self] response in
            switch response {
            case .success(let result):
                guard let self = self, let item = result.results else { return }
                let models = self.provider.createContentModels(with: item, delegate: self)
                self.view?.didObtainContentModel(models: models)
            case .failure(let error):
                print(error)
            }
        }
    }

}


// MARK: - ContentCardControllDelegate

extension ContentCardPresenter: CardInfoDelegate {
    
    func obtainMetaInfo(with filename: String, type: ContentType) {

        self.view?.updateContent(with: filename,
                                 type: type.localizedDescription)
    }

}


