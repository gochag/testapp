//
//  ContentCardProvider.swift
//  TestApp
//
//  Created by Tarlan Hekimzade on 10.10.2021.
//

import Foundation

protocol ContentCardProvider {
    func createContentModels(with item: ItemResultModel, delegate: CardInfoDelegate) -> [ContentModel]
}

enum ContentType {
    case video
    case photo
    
    var localizedDescription: String {
        switch self {
        case .video:
            return "Видео"
        case .photo:
            return "Фото"
        }
    }
}

struct ContentModel {
    let url: URL
    let name: String
    let type: ContentType
    var delegate: CardInfoDelegate
}

final class ContentCardProviderImp: ContentCardProvider {
    
    func createContentModels(with item: ItemResultModel, delegate: CardInfoDelegate) -> [ContentModel] {
        var contents = [ContentModel]()
        
        if let single = item.single {
            guard let singleUrl = URL(string: single) else { return contents }
            let model = ContentModel(url: singleUrl,
                                     name: "single",
                                     type: .video,
                                     delegate: delegate)
            contents.append(model)
        }

        if let splitVertical = item.splitVertical {
            guard let splitVerticalUrl = URL(string: splitVertical) else { return contents }
            let model = ContentModel(url: splitVerticalUrl,
                                     name: "splitVertical",
                                     type: .video,
                                     delegate: delegate)
            contents.append(model)
        }

        if let splitHorizontal = item.splitHorizontal {
            guard let splitHorizontalURL = URL(string: splitHorizontal) else { return contents }
            let model = ContentModel(url: splitHorizontalURL,
                                     name: "splitHorizontal",
                                     type: .video,
                                     delegate: delegate)
            contents.append(model)
        }
        
        if let src = item.src {
            guard let srcURL = URL(string: src) else { return contents }
            let model = ContentModel(url: srcURL,
                                     name: "src",
                                     type: .video,
                                     delegate: delegate)
            contents.append(model)
        }
        
        if let previewImage = item.previewImage {
            guard let previewImageURL = URL(string: previewImage) else { return contents }
            let model = ContentModel(url: previewImageURL,
                                     name: "previewImage",
                                     type: .photo,
                                     delegate: delegate)
            contents.append(model)
        }
        
        return contents
    }
}
