//
//  ContentInfo.swift
//  TestApp
//
//  Created by Tarlan Hekimzade on 10.10.2021.
//

import Foundation

struct ContentInfo {
    let fileName: String
    let path: URL
    let siz: Int
    let type: ContentType
    let duration: Int = 0
    let width: Int
    let height: Int
}
