//
//  ContentCardViewController.swift
//  TestApp
//
//  Created by Tarlan Hekimzade on 08.10.2021.
//

import UIKit


protocol ContentCardViewControllerIntput: AnyObject {
    func didObtainContentModel(models: [ContentModel])
    func didFailToObtainItem()
    func updateContent(with name: String, type: String)
}


final class ContentCardViewController: UIViewController {
    
    private lazy var pageView: PageViewController = {
        let pageView = PageViewController()
        pageView.pageDelegate = self
        return pageView
    }()
    
    private lazy var infoStack: UIStackView = {
        let stackView = UIStackView()
        stackView.distribution = .fillEqually
        stackView.alignment = .fill
        stackView.spacing = 8
        stackView.axis = .vertical
        stackView.translatesAutoresizingMaskIntoConstraints = false
        return stackView
    }()
    
    private lazy var contentTypeLabel: UILabel = {
        let label = UILabel()
        label.textColor = #colorLiteral(red: 0.8549019694, green: 0.250980407, blue: 0.4784313738, alpha: 1)
        label.font = UIFont.boldSystemFont(ofSize: 18)
        label.numberOfLines = 1
        return label
    }()
    
    private lazy var contentFileNameLabel: UILabel = {
        let label = UILabel()
        label.textColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        label.font = UIFont.boldSystemFont(ofSize: 18)
        label.numberOfLines = 1
        return label
    }()
    
    
    var presenter: ContentCardPresenterIntput?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.drawSelf()
        self.updateView()
        self.presenter?.obtainItem()
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        self.updateView()
    }
    
    
    // MARK: Private
    
    private func drawSelf() {
        self.view.backgroundColor = .black
        
        self.view.addSubview(self.pageView.view)

        self.view.addSubview(self.infoStack)
        
        self.infoStack.addArrangedSubview(self.contentTypeLabel)
        self.infoStack.addArrangedSubview(self.contentFileNameLabel)
        
        var constraints = [NSLayoutConstraint]()
        constraints += self.createConstraintForStack()
        
        NSLayoutConstraint.activate(constraints)
    }
    
    
    private func updateView() {
        self.pageView.view.frame = self.view.bounds
    }
    
    private func createConstraintForStack()  -> [NSLayoutConstraint] {
        [
            self.infoStack.bottomAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.bottomAnchor, constant: -16),
            self.infoStack.leftAnchor.constraint(equalTo: self.view.leftAnchor, constant: 16),
            self.infoStack.rightAnchor.constraint(equalTo: self.view.rightAnchor, constant: -16),
            self.infoStack.heightAnchor.constraint(equalToConstant: 75)
        ]
    }

}


// MARK: - PageDelegate

extension ContentCardViewController: PageDelegate {
    
    func didPresent(page: UIViewController, at index: Int) {
        print("delegate index:", index)
    }

}


//MARK: - ContentCardViewControllerIntput

extension ContentCardViewController: ContentCardViewControllerIntput {
    
    func didObtainContentModel(models: [ContentModel]) {
        
        self.pageView.pages = models.map {
            let pageModel = CardViewAssembly.Model(contentModel: $0)
            return CardViewAssembly.buildModule(with: pageModel)
        }
    }
    
    func didFailToObtainItem() {
        
    }
    
    func updateContent(with name: String, type: String) {
        self.contentTypeLabel.text = type
        self.contentFileNameLabel.text = name
    }
    
}
