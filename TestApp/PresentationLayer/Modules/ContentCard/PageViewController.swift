//
//  PageViewController.swift
//  TestApp
//
//  Created by Tarlan Hekimzade on 10.10.2021.
//

import UIKit

protocol PageDelegate: AnyObject {
    func didPresent(page: UIViewController, at index: Int)
}

final class PageViewController: UIPageViewController {
    
    var selectedIndex: Int = 0 {
        didSet {
            guard
                self.selectedIndex >= 0,
                self.selectedIndex < pages.count,
                self.selectedIndex != oldValue else { return }
            self.changeFocus(from: oldValue, to: self.selectedIndex)
        }
    }
    
    var pageDelegate: PageDelegate?
    
    lazy var pages: [UIViewController] = [] {
        didSet {
            guard !self.pages.isEmpty else { return }
            self.setViewControllers([pages[0]], direction: .forward, animated: true, completion: nil)
            self.active(to: 0)
        }
    }
    
    
    override init(transitionStyle style: UIPageViewController.TransitionStyle, navigationOrientation: UIPageViewController.NavigationOrientation, options: [UIPageViewController.OptionsKey : Any]? = nil) {
        super.init(transitionStyle: .scroll, navigationOrientation: .horizontal, options: nil)
        self.dataSource = self
        self.delegate = self
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}


//MARK: - UIPageViewControllerDataSource

extension PageViewController: UIPageViewControllerDataSource {
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        if let index = self.pages.firstIndex(of: viewController) {
            
            if index > 0 {
                let newIndex = index - 1
                return self.pages[newIndex]
            }
        }
        
        return nil
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        if let index = self.pages.firstIndex(of: viewController) {
            
            if index + 1 < self.pages.count {
                let newIndex = index + 1
                return self.pages[newIndex]
            }
        }
        
        return nil
    }
    
    
    private func changeFocus(from: Int, to: Int) {
        self.active(to: to)
        self.deactive(from: from)
    }
    
    private func active(to: Int) {
        guard let cardView = self.pages[to] as? CardViewController else {
            return }
        print("==> play to: \(to)")
        cardView.playVideo()
        cardView.updateCardInfo()
    }
    
    private func deactive(from: Int) {
        guard let cardView = self.pages[from] as? CardViewController else {
            return }
        
        print("==> stop from: \(from)")
        cardView.stopVideo()
    }
}


//MARK: - UIPageViewControllerDelegate

extension PageViewController: UIPageViewControllerDelegate {
    
    func pageViewController(_ pageViewController: UIPageViewController, didFinishAnimating finished: Bool,
                            previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
        
        
        if !completed {
            
            guard let presentingViewController = previousViewControllers.first,
                  let index = self.pages.firstIndex(of: presentingViewController) else { return }
            
            self.selectedIndex = index
            self.pageDelegate?.didPresent(page: self.pages[index], at: index)
            return
        }
        
        if finished {
            self.pageDelegate?.didPresent(page: self.pages[self.selectedIndex], at: selectedIndex)
        }
        
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, willTransitionTo pendingViewControllers: [UIViewController]) {
        
        guard let presentingViewController = pendingViewControllers.first,
              let index = self.pages.firstIndex(of: presentingViewController) else { return }
        
        self.selectedIndex = index
    }
    
    
}


