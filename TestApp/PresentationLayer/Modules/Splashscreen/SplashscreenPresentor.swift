//
//  SplashscreenPresentor.swift
//  TestApp
//
//  Created by Tarlan Hekimzade on 09.10.2021.
//

import Foundation

protocol SplashscreenPresentorIntput: AnyObject {
    func loading()
}


class SplashscreenPresentor {
    
    weak var view: SplashscreenViewIntput?
    
    private let networkService: NetworkService
    private let delegate: SceneDelegateIntput

    
    init(networkService: NetworkService, delegate: SceneDelegateIntput) {
        self.networkService = networkService
        self.delegate = delegate
    }
    
    
}

extension SplashscreenPresentor: SplashscreenPresentorIntput {
    
    func loading() {
        DispatchQueue.main.asyncAfter(deadline: .now() + 3) {
            self.delegate.setStartScreent()
        }
    }
}
