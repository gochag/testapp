//
//  SplashscreenAssembly.swift
//  TestApp
//
//  Created by Tarlan Hekimzade on 09.10.2021.
//

import UIKit

final class SplashscreenAssembly: Assembly {
    
    struct Model: AssemblyModel {
        let delegate: SceneDelegateIntput
    }
    
    static func buildModule(with model: AssemblyModel) -> UIViewController {
        
        guard let model = model as? Model else { fatalError() }
        
        let networkService = NetworkService()
        
        let viewController = SplashscreenViewController()
        let presentor = SplashscreenPresentor(networkService: networkService, delegate: model.delegate)
        viewController.presentor = presentor
        
        presentor.view = viewController
        
        return viewController
    }
    
}
