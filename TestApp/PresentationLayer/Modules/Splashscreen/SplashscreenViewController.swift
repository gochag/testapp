//
//  ViewController.swift
//  TestApp
//
//  Created by Tarlan Hekimzade on 06.10.2021.
//

import UIKit

protocol SplashscreenViewIntput: AnyObject {
    func didObtainItem()
    func didFailToObtainItem()
}


class SplashscreenViewController: UIViewController {
    
    var presentor: SplashscreenPresentorIntput?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = .gray
        self.presentor?.loading()
    }
    
}


//MARK: SplashscreenViewIntput

extension SplashscreenViewController: SplashscreenViewIntput {
    
    func didObtainItem() {

    }
    
    func didFailToObtainItem() {
        
    }
    
}
